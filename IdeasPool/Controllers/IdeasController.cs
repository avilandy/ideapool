﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using IdeasPool.Classes;
using IdeasPool.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IdeasPool.Controllers
{
    [Produces("application/json")]
    [Route("ideas/")]
    [ValidateModel]
    public class IdeasController : Controller
    {
        private readonly IdeaContext _context;

        public IdeasController(IdeaContext context)
        {
            _context = context;
        }

        [HttpGet("{page:int?}")]
        public IActionResult GetPage(int page = 1)
        {
            if (!User.Identity.IsAuthenticated)
                return new JsonResult(new { error = "User is unauthorized"}) { StatusCode = (int)HttpStatusCode.Unauthorized };

            var pageSize = 10;
            if (_context.IdeasPool.Count() < (page - 1) * pageSize)
                return new JsonResult(new { error = "Couldn't find page " + page }) { StatusCode = (int)HttpStatusCode.NotFound };

            return Json( _context.IdeasPool.OrderBy(idea => idea.Average_score).Skip((page - 1) * pageSize).Take(pageSize));
        }

        [HttpPost]
        public IActionResult Create([FromBody] Idea item)
        {
            if (!User.Identity.IsAuthenticated)
                return new JsonResult(new { error = "User is unauthorized" }) { StatusCode = (int)HttpStatusCode.Unauthorized };


            _context.IdeasPool.Add(item);
            _context.SaveChanges();

            return new ObjectResult(item) { StatusCode = (int)HttpStatusCode.Created };
        }

        //[HttpPost]
        //public IActionResult Create([FromBody] Idea[] items)
        //{
        //    if (!User.Identity.IsAuthenticated)
        //        return new JsonResult(new { error = "User is unauthorized" }) { StatusCode = (int)HttpStatusCode.Unauthorized };

        //    items.ToList().ForEach(item => _context.IdeasPool.Add(item));
        //    _context.SaveChanges();

        //    return new ObjectResult(items) { StatusCode = (int)HttpStatusCode.Created };
        //}

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Idea item)
        {
            if (!User.Identity.IsAuthenticated)
                return new JsonResult(new { error = "User is unauthorized"}) { StatusCode = (int)HttpStatusCode.Unauthorized };

            if (_context.IdeasPool == null)
                return new JsonResult(new { error = "Couldn't find item " + id }) { StatusCode = (int)HttpStatusCode.NotFound };

            var originalItem = _context.IdeasPool.FirstOrDefault(t => t.ID == id);

            if (originalItem == null)
                return new JsonResult(new { error = "Couldn't find item " + id }) { StatusCode = (int)HttpStatusCode.NotFound };


            originalItem.Update(item);
            _context.SaveChanges();

            return new JsonResult(originalItem);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!User.Identity.IsAuthenticated)
                return new JsonResult(new { error = "User is unauthorized"}) { StatusCode = (int)HttpStatusCode.Unauthorized };

            if (_context.IdeasPool == null)
                return new JsonResult(new { error = "Couldn't find item " + id }) { StatusCode = (int)HttpStatusCode.NotFound };

            var originalItem = _context.IdeasPool.FirstOrDefault(t => t.ID == id);

            if (originalItem == null)
                return new JsonResult(new { error = "Couldn't find item " + id }) { StatusCode = (int)HttpStatusCode.NotFound };

            _context.IdeasPool.Remove(originalItem);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}