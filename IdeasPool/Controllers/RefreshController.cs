﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using IdeasPool.Classes;
using IdeasPool.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace IdeasPool.Controllers
{
    [Produces("application/json")]
    public class RefreshController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly Auth0Settings _auth0Settings;


        public RefreshController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IOptions<Auth0Settings> settings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _auth0Settings = settings.Value;
        }

        [HttpPost("access-tokens/refresh")]
        public IActionResult Refresh([FromBody] TokenRefresh tokenRefresh)
        {
            var token = Request.Headers["X-Access-Token"].FirstOrDefault();

            if (token == null)
                return new JsonResult(new { error = "User is unauthorized"}) { StatusCode = (int)HttpStatusCode.Unauthorized };

            var securityToken = new JwtSecurityTokenHandler().ReadToken(token) as JwtSecurityToken;

            var user = _userManager.FindByIdAsync(securityToken.Id).Result;
            var refreshToken = _userManager.GetAuthenticationTokenAsync(user, "Internal", "RefreshToken").Result;
            var refreshTokenExpiration = _userManager.GetAuthenticationTokenAsync(user, "Internal", "RefreshTokenExpiration").Result;
            if (refreshToken != tokenRefresh.Refresh_Token)
                return new JsonResult(new {Error = "Refresh Token doesn't match" }) { StatusCode = (int)HttpStatusCode.BadRequest };

            if (Convert.ToDateTime(refreshTokenExpiration) < DateTime.Now)
                return new JsonResult(new { Error = "The Refresh Token is expired" }) { StatusCode = (int)HttpStatusCode.Unauthorized };


             return Ok(JwtToken.GenerateJwtToken(user.Email, user, _userManager, _signInManager, _auth0Settings, false));
        }
    }
}