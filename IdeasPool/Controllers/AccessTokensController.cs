﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using IdeasPool.Classes;
using IdeasPool.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace IdeasPool.Controllers
{
    [Produces("application/json")]
    public class AccessTokensController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly Auth0Settings _auth0Settings;


        public AccessTokensController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IOptions<Auth0Settings> settings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _auth0Settings = settings.Value;
        }

        [HttpPost]
        public async Task<IActionResult> Process([FromBody] LoginDto model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

            if (result.Succeeded)
            {
                var appUser = _userManager.Users.SingleOrDefault(r => r.Email == model.Email);
                return new JsonResult(JwtToken.GenerateJwtToken(model.Email, appUser, _userManager, _signInManager, _auth0Settings)) { StatusCode = (int)HttpStatusCode.Created };
            }

            return new JsonResult(new { Error = "Incorrect username or password" }) { StatusCode = (int)HttpStatusCode.BadRequest };

        }

        [HttpDelete]
        public IActionResult Process([FromBody] TokenRefresh tokenRefresh)
        {
            if (!User.Identity.IsAuthenticated)
                return new JsonResult(new { error = "User is unauthorized"}) { StatusCode = (int)HttpStatusCode.Unauthorized };

            var user = _userManager.GetUserAsync(User).Result;
            _userManager.RemoveAuthenticationTokenAsync(user, "Internal", "RefreshToken");
            _userManager.RemoveAuthenticationTokenAsync(user, "Internal", "RefreshTokenExpiration");
            _signInManager.SignOutAsync();
            return NoContent();
        }

        public class LoginDto
        {
            [Required]
            public string Email { get; set; }

            [Required]
            public string Password { get; set; }

        }
    }
}