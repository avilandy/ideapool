﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using IdeasPool.Classes;
using IdeasPool.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace IdeasPool.Controllers
{
    [Produces("application/json")]
    public class MeController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly Auth0Settings _auth0Settings;

        public MeController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IOptions<Auth0Settings> settings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _auth0Settings = settings.Value;
        }

        [HttpGet]
        public async Task<IActionResult> Me()
        {
            if (!User.Identity.IsAuthenticated)
                return new JsonResult(new { error = "User is unauthorized"}) { StatusCode = (int)HttpStatusCode.Unauthorized };

            var user = await GetCurrentUser();
            
            return Ok(new UserResponse()
            {
                Email = user.Email,
                Name = user.Name,
                Avatar_Url = "https://www.gravatar.com/avatar/" + BitConverter.ToString(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(user.Email.Trim().ToLower()))).Replace("-", string.Empty)

            });
        }   

        private async Task<ApplicationUser> GetCurrentUser()
        {
            var token = Request.Headers["X-Access-Token"].FirstOrDefault();

            if (token == null)
                return null;

            var securityToken = new JwtSecurityTokenHandler().ReadToken(token) as JwtSecurityToken;

            return await _userManager.FindByIdAsync(securityToken.Id);
        }
    }
}