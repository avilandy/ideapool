﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IdeasPool.Classes;
using IdeasPool.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace IdeasPool.Controllers
{
    [Produces("application/json")]
    public class UsersController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IOptions<Auth0Settings> _settings;
        private readonly Auth0Settings _auth0Settings;

        public UsersController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IOptions<Auth0Settings> settings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _settings = settings;
            _auth0Settings = settings.Value;
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterDto model)
        {
            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                Name = model.Name
            };


            var result = _userManager.CreateAsync(user, model.Password).Result;

            if (result.Succeeded)
            {
                var loginController = new AccessTokensController(_userManager, _signInManager, _settings);
                loginController.ControllerContext = new ControllerContext(this.ControllerContext);
                return await loginController.Process(new AccessTokensController.LoginDto() { Email = model.Email, Password = model.Password });
            }

            else
                return new ContentResult() { StatusCode = (int)HttpStatusCode.BadRequest, Content = SimpleJson.SimpleJson.SerializeObject(result.Errors.Select(e => e.Description))};
        }

        public class RegisterDto
        {
            [Required]
            public string Email { get; set; }

            [Required]
            public string Name { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 8)]
            public string Password { get; set; }
        }
    }
}
