﻿using IdeasPool.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IdeasPool.Classes
{
    public class JwtToken
    {
        public static object GenerateJwtToken(string email, ApplicationUser user, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, Auth0Settings auth0Settings, bool generateRefreshToken = true)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, user.Id),
                new Claim(ClaimTypes.NameIdentifier, user.UserName)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(auth0Settings.ClientSecret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(10);

            var token = new JwtSecurityToken(
                auth0Settings.Domain,
                auth0Settings.Domain,
                claims,
                expires: expires,
                signingCredentials: creds
            );


            if (generateRefreshToken)
            {
                var refreshToken = Guid.NewGuid().ToString("N") + Guid.NewGuid().ToString("N");
                userManager.SetAuthenticationTokenAsync(user, "Internal", "RefreshToken", refreshToken);
                userManager.SetAuthenticationTokenAsync(user, "Internal", "RefreshTokenExpiration", DateTime.Now.AddYears(30).ToString());

                return new
                {
                    jwt = new JwtSecurityTokenHandler().WriteToken(token),
                    refresh_token = refreshToken
                };
            }

            else
                return new
                {
                    jwt = new JwtSecurityTokenHandler().WriteToken(token)
                };
        }

        public SecurityToken GetCurrentToken(string token, Auth0Settings auth0Settings)
        {
            try
            {
                //var b64secret = settings.Value.ClientSecret.Replace('_', '/').Replace('-', '+');
                var secret = Convert.FromBase64String(auth0Settings.ClientSecret);
                var tokenHandler = new JwtSecurityTokenHandler();
                SecurityToken securityToken;
                var principle = tokenHandler.ValidateToken(token, null, out securityToken);
                return securityToken;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
