﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IdeasPool.Classes
{
    public class JsonWebTokenAuthorization
    {
        private readonly RequestDelegate next;
        private Auth0Settings _settings = null;
        public JsonWebTokenAuthorization(RequestDelegate next, IOptions<Auth0Settings> settings)
        {
            this.next = next;
            _settings = settings.Value;
        }

        public Task Invoke(HttpContext context)
        {
            if (context.Request.Headers.ContainsKey("x-access-token"))
            {
                var token = context.Request.Headers["x-access-token"].First();
                SecurityToken securityToken;
                var validationParameters = new TokenValidationParameters()
                {
                    ValidAudience = _settings.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.ClientSecret)),
                    ValidIssuer = _settings.Domain
                };

                try
                {
                    context.User = new JwtSecurityTokenHandler().ValidateToken(token, validationParameters, out securityToken);
                }
                catch (SecurityTokenExpiredException)
                {
                }
                //Debug.WriteLine(string.Format("[JsonWebTokenAuthorization] JWT Decoded as {0}", claims));
            }
            return next(context);
        }

        private static void ValidateJwtWithHs256(String encodedJwt, String base64EncodedSecret, String validAudience, String validIssuer)
        {
            //var tokenValidationParameters = new TokenValidationParameters
            //{
            //    IssuerSigningToken = new BinarySecretSecurityToken(Base64UrlEncoder.DecodeBytes(base64EncodedSecret)),
            //    ValidIssuer = validIssuer,
            //    ValidAudience = validAudience,
            //};
            
            //new JwtSecurityTokenHandler().ValidateToken(encodedJwt, tokenValidationParameters, out securityToken);
        }
    }
}
