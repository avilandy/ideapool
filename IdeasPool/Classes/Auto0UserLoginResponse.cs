﻿using IdeasPool.Models;
using Microsoft.Extensions.Options;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdeasPool.Classes
{
    public class Auth0UserHelper
    {

        public static UserToken GetUserToken(Auth0Settings settings, string userName, string password)
        {
            return Auth0Transmission(settings, string.Format("{{\"client_id\":\"{0}\",\"client_secret\":\"{1}\",\"grant_type\":\"password\",\"scope\":\"offline_access\",\"username\":\"{2}\",\"password\":\"{3}\",\"audience\":\"{4}\"}}",
                                                settings.ClientId, settings.ClientSecret, userName, password, settings.Audience));
        }

        public static UserToken RefreshToken(Auth0Settings settings, UserToken originalToken)
        {
            return Auth0Transmission(settings, string.Format("{{\"client_id\":\"{0}\",\"client_secret\":\"{1}\",\"grant_type\":\"refresh_token\",\"scope\":\"offline_access\",\"refresh_token\":\"{2}\",\"audience\":\"{3}\"}}",
                                                settings.ClientId, settings.ClientSecret, originalToken.refresh_token, settings.Audience));

        }

        private static UserToken Auth0Transmission(Auth0Settings settings, string payload)
        {
            var client = new RestClient(string.Format("https://{0}/oauth/token", settings.Domain));
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", payload, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return SimpleJson.SimpleJson.DeserializeObject<UserToken>(response.Content);
        }
    }
}
