﻿using Microsoft.Extensions.Options;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdeasPool.Classes
{
    public class Auto0AuthenticationResponse
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }

        public static string GetManagementToken(Auth0Settings settings)
        {
            var client = new RestClient(string.Format("https://{0}/oauth/token", settings.Domain));
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", string.Format("{{\"client_id\":\"{0}\",\"client_secret\":\"{1}\",\"audience\":\"https://{2}/api/v2/\",\"grant_type\":\"client_credentials\"}}",
                                                settings.ClientId, settings.ClientSecret, settings.Domain), ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return SimpleJson.SimpleJson.DeserializeObject<Auto0AuthenticationResponse>(response.Content).access_token;
        }
    }
}
