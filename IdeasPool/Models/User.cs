﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdeasPool.Models
{
    public class User
    {
        public string ID { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Avatar_Url { get; set; }
    }

    public class UserResponse
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Avatar_Url { get; set; }
    }
}
