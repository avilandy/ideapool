﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdeasPool.Models
{
    public class TokenRefresh
    {
        [Required]
        public string Refresh_Token { get; set; }

    }
}
