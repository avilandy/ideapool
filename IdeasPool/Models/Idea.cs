﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdeasPool.Models
{
    public class Idea
    {
        private static DateTime _epoch = new DateTime(1970, 1, 1,0,0,0, DateTimeKind.Utc);

        private DateTime _createdAt;
        public Idea()
        {
            _createdAt = DateTime.Now;
        }
        public int ID { get; set; }
        [Required(AllowEmptyStrings = false)]
        [MaxLength(255)]
        public string Content { get; set; }
        [Required]
        [Range(1, 10)]
        public int? Impact { get; set; }
        [Required]
        [Range(1, 10)]
        public int? Ease { get; set; }
        [Required]
        [Range(1, 10)]
        public int? Confidence { get; set; }

        public decimal Average_score
        {
            get
            {
                if (!Impact.HasValue || !Ease.HasValue || !Confidence.HasValue)
                    return 0;

                return  (Impact.Value + Ease.Value + Confidence.Value) / 3M;
            }
        }

        public long Created_at { get
            {
                return (long)(_createdAt - _epoch).TotalSeconds;
            }
        }


        internal void Update(Idea item)
        {
            Content = item.Content;
            Impact = item.Impact;
            Ease = item.Ease;
            Confidence = item.Confidence;
        }
    }
}
