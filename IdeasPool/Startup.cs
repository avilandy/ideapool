﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using IdeaPool2.Data.Migrations;
using IdeasPool.Classes;
using IdeasPool.Controllers;
using IdeasPool.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace IdeasPool
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("UsersContext")));

            services.AddDbContext<IdeaContext>(options =>
                options.UseInMemoryDatabase("Ideas"));

            var auth0config = Configuration.GetSection("Auth0");
            services.Configure<Auth0Settings>(auth0config);

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
               // app.UseDeveloperExceptionPage();
            }
            app.UseExceptionHandler(
             options =>
             {
                 options.Run(
                 async context =>
                 {
                     context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                     context.Response.ContentType = "application/json";
                     ExceptionHandlerFeature ex = context.Features.Get<IExceptionHandlerFeature>() as ExceptionHandlerFeature;
                     if (ex != null)
                     {
                         var error = SimpleJson.SimpleJson.SerializeObject(new { ex.Path, Error = ex.Error.Message + "\n" + ex.Error.StackTrace });
                         await context.Response.WriteAsync(error).ConfigureAwait(false);
                     }
                 });
             }
            );

            app.UseCors(options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials());

            app.UseMiddleware<JsonWebTokenAuthorization>();
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "Me",
                    template: "me",
                    defaults: new { controller = "Me", action = "Me" });

                routes.MapRoute(
                    name: "register",
                    template: "users",
                    defaults: new { controller = "Users", action = "Register" });

                routes.MapRoute(
                   name: "refresh",
                   template: "access-tokens/refresh",
                   defaults: new { controller = "Refresh", action = "Refresh" });

                routes.MapRoute(
                   name: "AccessTokens",
                   template: "access-tokens",
                   defaults: new { controller = "AccessTokens", action="Process" });


                routes.MapRoute(
                   name: "default",
                   template: "{controller}/{action}");

            });
        }
    }
}
